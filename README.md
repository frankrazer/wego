## Introduction
Simple Coffee Shop App setup. You might want to install [docker](https://docs.docker.com/installation/) and make (apt-get install make) first

## Build and Run

You might need to point BACKEND_URL into your machine IP at frontend/js/custom-angular.js

```sh
git clone https://princep3@bitbucket.org/princep3/wego.git
cd wego

# Run backend
make run_backend

# Run frontend
make run_frontend

```

## Demo
http://trananhcuong.com:8000/#/

## Contacts
[Frank Tran](https://bitbucket.org/PrinceP3)
